﻿using Demo.ApiCore.EF.Context;
using Demo.ApiCore.EF.Entities;
using System;
using System.Linq;

namespace Demo.EF
{
    class Program
    {
        static void Main(string[] args)
        {
            using (MoviesContext dc = new MoviesContext())
            {
                //dc.Movies.Add(new Movie
                //{
                //    Title = "Star wars 5",
                //    Synopsis = "Ca commence dans la neige",
                //    RealeaseDate = new DateTime(1979, 10, 27),
                //    CategoryId = 1
                //});

                //dc.SaveChanges();

                foreach(Movie m in dc.Movies.Where(x => x.Id == 1).ToList())
                {
                    Console.WriteLine(m.Category.Name);
                    Console.WriteLine(m.Category.Description);
                }
            }
            Console.ReadKey();
        }
    }
}
