﻿using Demo.ApiCore.EF.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Demo.ApiCore.EF.Context
{
    public class MoviesContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Category> Categories { get; set; }

        public MoviesContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseLazyLoadingProxies();
            //options.UseSqlServer(@"server=K-LAPTOP\SQLSERVER;initial catalog=efmovie;integrated security=sspi");
            options.UseSqlServer(@"server=K-LAPTOP\SQLSERVER;initial catalog=efmovie;uid=sa;pwd=test1234=");
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Movie>(e => {
                e.HasKey(x => x.Id);

                e.HasIndex(x => x.Title).IsUnique();

                e.Property(x => x.Title)
                    .IsRequired()
                    .HasMaxLength(100);

                e.Property(x => x.Synopsis)
                    .HasMaxLength(1000);

                e.HasOne(x => x.Category)
                    .WithMany(c => c.Movies)
                    .HasForeignKey(x => x.CategoryId)
                    .OnDelete(DeleteBehavior.SetNull);
            });

            builder.Entity<Category>(e =>
            {
                e.HasKey(x => x.Id);

                e.HasIndex(x => x.Name).IsUnique();

                e.Property(x => x.Name).HasMaxLength(50).IsRequired();
            });
        }
    }
}
