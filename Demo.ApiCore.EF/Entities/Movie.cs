﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo.ApiCore.EF.Entities
{
    public class Movie
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Synopsis { get; set; }
        public DateTime RealeaseDate { get; set; }
        public int? CategoryId { get; set; }
        public virtual Category Category { get; set; }
    }
}
