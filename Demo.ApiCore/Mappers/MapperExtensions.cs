﻿using Demo.ApiCore.EF.Entities;
using Demo.ApiCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.ApiCore.Mappers
{
    public static class MapperExtensions
    {
        public static MovieDTO ToDTO(this Movie from)
        {
            return new MovieDTO
            {
                Id = from.Id,
                Title = from.Title,
                Synopsis = from.Synopsis,
                CategoryName = from.Category.Name
            };
        }
    }
}
