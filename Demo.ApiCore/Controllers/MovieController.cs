﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.ApiCore.EF.Context;
using Demo.ApiCore.EF.Entities;
using Demo.ApiCore.Mappers;
using Demo.ApiCore.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Demo.ApiCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieController : ControllerBase
    {
        private MoviesContext _dc;

        public MovieController(MoviesContext dc)
        {
            _dc = dc;
        }

        public IEnumerable<MovieDTO> Get()
        {
            return _dc.Movies.ToList().Select(m => m.ToDTO());
        }
    }
}
